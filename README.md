# MYSQL driver for laravel scout

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email security@slym.io instead of using the issue tracker.

## Credits

- [Yannik Firre](https://github.com/YannikFirre)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
