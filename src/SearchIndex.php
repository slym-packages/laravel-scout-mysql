<?php

namespace Slym\LaravelScoutMysql;

use Illuminate\Database\Eloquent\Model;

class SearchIndex extends Model
{
    protected $table = 'search_index';

    protected $fillable = [
        'record_type',
        'record_id',
        'field',
        'weight',
        'content',
        'locale'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function record()
    {
        return $this->morphTo();
    }
}
