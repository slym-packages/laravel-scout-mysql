<?php

namespace Slym\LaravelScoutMysql;

use Illuminate\Support\ServiceProvider;
use Laravel\Scout\EngineManager;

class LaravelScoutMysqlServiceProvider extends ServiceProvider
{
    public function boot()
    {
        app(EngineManager::class)->extend('mysql', function () {
            return new Engine();
        });

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../database/migrations/' => database_path('migrations'),
            ], 'migrations');
        }

        $this->publishes([
            __DIR__.'/../config/scout-mysql.php' => $this->app['path.config'].DIRECTORY_SEPARATOR.'scout-mysql.php',
        ]);
    }
}
