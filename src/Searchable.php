<?php

namespace Slym\LaravelScoutMysql;

use Laravel\Scout\Searchable as ScoutSearchable;

trait Searchable
{
    use ScoutSearchable;

    /**
     * Perform a search against the model's indexed data.
     *
     * @param  string  $query
     * @param  \Closure  $callback
     * @return \Laravel\Scout\Builder
     */
    public static function search($query = '', $callback = null)
    {
        return app(Builder::class, [
            'model' => new static,
            'query' => $query,
            'callback' => $callback,
            'softDelete'=> static::usesSoftDelete() && config('scout.soft_delete', false),
        ]);
    }

    /**
     * Get the weight of the specified field.
     *
     * @param string $field
     * @return int
     */
    public function getSearchWeight($field)
    {
        return $this->weights[$field] ?? 1;
    }

    public function toSearchableArray(): array
    {
        $array = [];

        if(isset($this->translatable)) {
            $array[0] = $this->getTranslations();
        }

        foreach ($this->fillable as $key) {

            if(in_array($key, $this->translatable, true)) {
                continue;
            }

            $value = $this->{$key};

            if(is_string($value)) {
                $array[1][$key] = $value;
            }
        }

        return array_merge([], ...$array);
    }

    public function toSearchableArrayWithPrefix(string $prefix): array {
        $array = $this->toSearchableArray();

        return array_combine(
            array_map(static function($key) use ($prefix) { return $prefix.'_'.$key; }, array_keys($array)),
            $array
        );
    }

    public function searchIndices() {
        return $this->morphMany(SearchIndex::class, 'record');
    }
}
