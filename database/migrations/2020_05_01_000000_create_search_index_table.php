<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchIndexTable extends Migration
{
    public function up()
    {
        Schema::create('search_index', function (Blueprint $table) {
            $table->id();
            $table->string('record_type', 191)->index();
            $table->unsignedBigInteger('record_id')->index();
            $table->string('field', 191)->index();
            $table->unsignedSmallInteger('weight')->default(1);
            $table->text('content');
            $table->char('locale', 2)->nullable();
            $table->timestamps();
        });
        $tableName = DB::connection()->getTablePrefix() . 'search_index';
        DB::connection()->statement("ALTER TABLE $tableName ADD FULLTEXT searchindex_content (content)");
    }


    public function down()
    {
        Schema::dropIfExists('search_index');
    }
}
