<?php

return [

        'default_mode' => 'in natural language mode',
        'filters' => [
            'strip_tags',
            'html_entity_decode',
            'mb_strtolower',
            'strip_punctuation',
            'remove_accents'
        ],
        'token_delimiter' => '/[\s]+/',
        'minimum_length' => 2,
        'stopwords' => [
            'fr' => []
        ],
       // 'stemmer' => Wamania\Snowball\French::class, // from package wamania/php-stemmer
];
